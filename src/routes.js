import {
  createRouter,
  createWebHistory,
  createWebHashHistory,
} from "vue-router";

const routerHistory = createWebHistory();

import HomePage from "./pages/home.vue";
import NotFoundPage from "./pages/notFound.vue";

const routes = createRouter({
  history: routerHistory,
  routes: [
    {
      path: "/",
      name: "home",
      component: HomePage,
    },
    {
      path: "/:CatchAll(.*)",
      name: "404",
      component: NotFoundPage,
    },
  ],
});

export default routes;
